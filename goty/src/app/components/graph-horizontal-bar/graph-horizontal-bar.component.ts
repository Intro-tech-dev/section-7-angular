import { Component, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-graph-horizontal-bar',
  templateUrl: './graph-horizontal-bar.component.html',
  styleUrls: ['./graph-horizontal-bar.component.css']
})
export class GraphHorizontalBarComponent implements OnDestroy {

  @Input() results: any[] = [];
  /*results: any[] = [
    {
      "name": "Germany",
      "value": 8940000
    },
    {
      "name": "USA",
      "value": 5000000
    },
    {
      "name": "France",
      "value": 7200000
    },
    {
      "name": "Italy",
      "value": 6200000
    }
  ];*/

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Juegos';
  showYAxisLabel = true;
  yAxisLabel = 'Votos';

  colorScheme = 'nightLights';

  intervalo;

  constructor() {
    this.intervalo = setInterval(() => {
      console.log('tick');
    }, 1500);
  }
  
  ngOnDestroy(): void {
    clearInterval(this.intervalo);
  }

  onSelect(event) {
    console.log(event);
  }

}
