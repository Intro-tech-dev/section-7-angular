import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphHorizontalBarComponent } from './graph-horizontal-bar.component';

describe('GraphHorizontalBarComponent', () => {
  let component: GraphHorizontalBarComponent;
  let fixture: ComponentFixture<GraphHorizontalBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphHorizontalBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphHorizontalBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
