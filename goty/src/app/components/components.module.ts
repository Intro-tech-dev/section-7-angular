import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { NavbarComponent } from './navbar/navbar.component';
import { GraphHorizontalBarComponent } from './graph-horizontal-bar/graph-horizontal-bar.component';

@NgModule({
  declarations: [
    NavbarComponent,
    GraphHorizontalBarComponent
  ],
  exports: [
    NavbarComponent,
    GraphHorizontalBarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    NgxChartsModule
  ]
})
export class ComponentsModule { }
