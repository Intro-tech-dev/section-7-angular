// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'http://localhost:5000/fire-grafica-1214b/us-central1',
  firebase: {
    apiKey: 'AIzaSyCAP8Eh4mTJeXngmMYTlsd37FYDbWv_GNM',
    authDomain: 'fire-grafica-1214b.firebaseapp.com',
    databaseURL: 'https://fire-grafica-1214b.firebaseio.com',
    projectId: 'fire-grafica-1214b',
    storageBucket: 'fire-grafica-1214b.appspot.com',
    messagingSenderId: '702059457154',
    appId: '1:702059457154:web:62fb01a273cf896335a29b'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
